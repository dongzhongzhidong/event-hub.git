import { terser } from 'rollup-plugin-terser';
import { babel } from '@rollup/plugin-babel';
const common = [
    babel({
        presets: ['@babel/typescript'],
        extensions: ['.ts'],
        babelHelpers: 'bundled',
        exclude: ['node_modules'],
        plugins: [],
    }),
];

export default [
    {
        input: './src/index.ts',

        output: {
            file: './dist/EventHub.esm.js',
            format: 'es',
        },
        plugins: [...common, terser()],
    },
    {
        input: './src/index.ts',
        output: {
            file: './dist/EventHub.common.cjs',
            format: 'cjs',
            exports: 'default',
        },
        plugins: [...common, terser()],
    },
    {
        input: './src/index.ts',
        output: {
            file: './dist/EventHub.umd.js',
            format: 'umd',
            name: 'EventHub',
        },
        plugins: [...common, terser()],
    },
];
