const EventHub = require('../dist/EventHub.common.cjs');
const success = { success: true };
const tenArray = [...Array(10).keys()];
//! this 指向测试
const thisTest = new EventHub({
    testNormal() {
        return this;
    },
    testArrow: () => {
        return this;
    },
    testBinded: function () {
        return this;
    }.bind(success),
});

describe('this 测试', () => {
    test('普通函数 this 指向测试', (done) => {
        Promise.all(thisTest.emit('testNormal')).then(([res]) => {
            expect(res).toEqual(thisTest);
            done();
        });
    });
    test('箭头函数 this 指向测试', (done) => {
        Promise.all(thisTest.emit('testArrow')).then(([res]) => {
            expect(res).toEqual(this);
            done();
        });
    });
    test('已绑定函数 this 指向测试', (done) => {
        Promise.all(thisTest.emit('testBinded')).then(([res]) => {
            expect(res).toBe(success);
            done();
        });
    });
});
const destroyFuncs = tenArray.map((index) => {
    return index % 2 ? () => index : async () => index;
});
//! 事件绑定测试
const testEvent = new EventHub(
    {
        created() {
            return 'created';
        },
        mounted() {
            return this;
        },
        beforeDestroy: destroyFuncs,
        destroyed: [
            () => {
                throw new Error('这是测试中的正常报错');
            },
            ...destroyFuncs,
        ],
    },
    success,
);
describe('事件触发', () => {
    test('单事件触发测试', (done) => {
        Promise.all(testEvent.emit('created')).then(([res]) => {
            expect(res).toBe('created');
            done();
        });
    });
    test('指定 this 绑定', (done) => {
        Promise.all(testEvent.emit('mounted')).then(([res]) => {
            expect(res).toEqual(success);
            done();
        });
    });
    test('触发顺序测试', (done) => {
        Promise.all(testEvent.emit('beforeDestroy')).then((res) => {
            expect(res).toEqual(tenArray);
            done();
        });
    });
    test('once事件触发测试', (done) => {
        testEvent.once('once', async (...args) => args);
        Promise.all(testEvent.emit('once', [1, 2, 3])).then((res) => {
            expect(testEvent.all.get('once')).toEqual([]);
            done();
        });
    });
});
describe('处理测试', () => {
    test('删除事件测试', (done) => {
        // 删除其中一个事件，查看其他事件合成的结果
        testEvent.off('beforeDestroy', destroyFuncs[0]);
        Promise.all(testEvent.emit('beforeDestroy')).then(async (res) => {
            res = await Promise.all(res);
            expect(res.sort((a, b) => a - b).join('')).toBe('123456789');
            done();
        });
    });
    test('报错测试', (done) => {
        Promise.all(testEvent.emit('destroyed')).then(async (res) => {
            res = await Promise.all(res);

            expect(res).toEqual([null, ...Array(10).keys()]);
            done();
        });
    });
    test('删除所有事件', () => {
        testEvent.off('*');
        expect(testEvent.all.size).toBe(0);
    });
});

describe('缺漏测试', () => {
    test('单项事件挂载测试', () => {
        const test = () => '';
        testEvent.on('test', test);
        expect(testEvent.all.get('test')[0]).toEqual(test);
    });

    test('删除无效事件测试', () => {
        expect(testEvent.off('null')).toBe(false);
    });

    test('触发无效事件测试', (done) => {
        Promise.all(testEvent.emit('null')).then((res) => {
            expect(res).toEqual([]);
            done();
        });
    });
    const voidHub = new EventHub();
    test('空模板创建', () => {
        expect(voidHub.all.size).toBe(0);
    });
    test('单事件不绑定函数测试', () => {
        expect(() => {
            return voidHub.on('none');
        }).toThrow('请绑定事件函数');
    });
    test('模板测试不绑定函数测试', () => {
        expect(() => {
            return voidHub.on({ none: undefined });
        }).toThrow('请绑定事件函数');
    });
    test('事件名称数据类型错误', () => {
        expect(() => {
            return voidHub.on(23238237);
        }).toThrow('事件函数数据类型错误');
    });
});
