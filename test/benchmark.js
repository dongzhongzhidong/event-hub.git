import Benchmark from 'benchmark';
var suite = new Benchmark.Suite();
import EventHub from '../dist/EventHub.esm.js';
import mitt from 'mitt';
import event from 'events';
const emitter1 = mitt();
const emitter2 = new EventHub();
const emitter3 = new event();
function plus() {
    const result = async () => [...Array(10).keys()].reduce((a, b) => a + b, 0);

    return result;
}
function bindEvents(emitter) {
    [...Array(10)].forEach((i) => emitter.on('Test', plus()));
}
bindEvents(emitter1);
bindEvents(emitter2);
bindEvents(emitter3);
let collection = [];
suite
    .add('EventHub', function () {
        emitter1.emit('Test');
    })
    .add('Mitt', function () {
        emitter2.emit('Test');
    })

    // add listeners
    .on('cycle', function (event) {
        collection.push(String(event.target));
    })
    .on('complete', function () {
        console.log(collection);
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    // run async
    .run({ async: true });
