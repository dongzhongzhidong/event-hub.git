export declare type EventHandlerMap<Events, Handler> = Map<keyof Events | '*', Handler[]>;
declare type originEventType = string | symbol | number;
declare type EventTemplate<K extends originEventType, Handler = Function> = {
    [name in K]: Handler | Handler[];
};
/**
 * @license
 * Copyright 2021 KonghaYao 江夏尧 <dongzhongzhidong@qq.com>
 * SPDX-License-Identifier: MIT
 *
 * @description EventHub 是一个事件处理中心，用于事件的接收与派发，如果有特定的要求，请直接继承 EventHub类创建操作
 */
export default class EventHub<EventType extends originEventType, H extends Function, T = undefined, Events = Record<EventType, H>> {
    all: EventHandlerMap<Events, H>;
    bindThis: T | this;
    constructor(template?: EventTemplate<keyof Events, H>, bindThis?: T);
    /**
     * singleOn 是单个事件绑定函数，type 与 handle 函数一一对应
     */
    private singeOn;
    /**
     * on 函数重载，第一个参数可以为一个事件绑定对象，
     *    on({
     *          eventName: callback,
     *          eventName: [callback]
     *    })
     *    on(type,handler)
     */
    on<Key extends keyof Events>(type: Key, handler: H): void;
    on<Key extends keyof Events>(Template: EventTemplate<Key, H>): void;
    /**
     * off 函数 type 设置为 '*' 时删除所有函数
     */
    off<Key extends keyof Events>(type: Key, handler: H): false | void | H[];
    emit<Key extends keyof Events>(type: Key, ...eventParams: any[]): any[];
    once<Key extends keyof Events>(type: Key, handler: H): this;
}
export {};
