"use strict";
/**
 * @license
 * Copyright 2021 KonghaYao 江夏尧 <dongzhongzhidong@qq.com>
 * SPDX-License-Identifier: MIT
 *
 * @description EventHub 是一个事件处理中心，用于事件的接收与派发，如果有特定的要求，请直接继承 EventHub类创建操作
 */module.exports=class{all=new Map;constructor(t,n){this.bindThis=n||this,t&&this.on(t)}singeOn(t,n){const i=this.all.get(t);i?i.push(n):this.all.set(t,[n])}on(t,n){if("string"==typeof t&&"*"!==t){if(!(n instanceof Function))throw new Error("请绑定事件函数");this.singeOn(t,n)}else{if(!(t instanceof Object))throw new Error("事件函数数据类型错误");Object.entries(t).forEach((([t,n])=>{if(n instanceof Array)n.forEach((n=>this.singeOn(t,n)));else{if(!(n instanceof Function))throw new Error("请绑定事件函数");this.singeOn(t,n)}}))}return this}off(t,n){if("*"===t)return this.all.clear();{const i=this.all.get(t);return!(!i||!n)&&i.splice(i.indexOf(n)>>>0,1)}}emit(t,...n){const i=this.all.get(t);return i?i.map((t=>{try{return t.apply(this.bindThis,n)}catch(t){return console.warn("EventHub: ",t),null}})):[]}once(t,n){let i=(...s)=>{n.apply(this.bindThis,s),this.off(t,i)};return this.singeOn(t,i),this}};
