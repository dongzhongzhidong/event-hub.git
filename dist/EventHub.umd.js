!function(t,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(t="undefined"!=typeof globalThis?globalThis:t||self).EventHub=n()}(this,(function(){"use strict";
/**
     * @license
     * Copyright 2021 KonghaYao 江夏尧 <dongzhongzhidong@qq.com>
     * SPDX-License-Identifier: MIT
     *
     * @description EventHub 是一个事件处理中心，用于事件的接收与派发，如果有特定的要求，请直接继承 EventHub类创建操作
     */return class{all=new Map;constructor(t,n){this.bindThis=n||this,t&&this.on(t)}singeOn(t,n){const e=this.all.get(t);e?e.push(n):this.all.set(t,[n])}on(t,n){if("string"==typeof t&&"*"!==t){if(!(n instanceof Function))throw new Error("请绑定事件函数");this.singeOn(t,n)}else{if(!(t instanceof Object))throw new Error("事件函数数据类型错误");Object.entries(t).forEach((([t,n])=>{if(n instanceof Array)n.forEach((n=>this.singeOn(t,n)));else{if(!(n instanceof Function))throw new Error("请绑定事件函数");this.singeOn(t,n)}}))}return this}off(t,n){if("*"===t)return this.all.clear();{const e=this.all.get(t);return!(!e||!n)&&e.splice(e.indexOf(n)>>>0,1)}}emit(t,...n){const e=this.all.get(t);return e?e.map((t=>{try{return t.apply(this.bindThis,n)}catch(t){return console.warn("EventHub: ",t),null}})):[]}once(t,n){let e=(...i)=>{n.apply(this.bindThis,i),this.off(t,e)};return this.singeOn(t,e),this}}}));
